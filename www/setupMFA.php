<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module\authswitcher\Auth\Process\SwitchAuth;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, SwitchAuth::SETUP_MFA_TPL_URL);

if (empty($_GET['stateId'])) {
    exit;
}

$state = State::loadState($_GET['stateId'], 'authswitcher:authswitcher', true);
$t->data[SwitchAuth::PARAM_MFA_REDIRECT_URL] = $state[SwitchAuth::PARAM_MFA_REDIRECT_URL];

$t->show();
