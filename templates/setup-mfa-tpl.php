<?php

declare(strict_types=1);

use SimpleSAML\Module\authswitcher\Auth\Process\SwitchAuth;

$this->includeAtTemplateBase('includes/header.php');
?>

<div class="row">
  <div>
      <p><?php echo $this->t('{authswitcher:mfa:setup_mfa_text}'); ?></p>
      <?php if (!empty($this->data[SwitchAuth::PARAM_MFA_REDIRECT_URL])) { ?>
        <form action="<?php echo htmlspecialchars($this->data[SwitchAuth::PARAM_MFA_REDIRECT_URL]); ?>" method="GET">
          <input type="submit" class="btn btn-lg btn-primary"
                 value="<?php echo $this->t('{authswitcher:mfa:manage_tokens_button}'); ?>">
        </form>
      <?php } ?>
  </div>
</div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
?>
